# justAsimpleThing

[GET] /artikels : menampilkan seluruh artikel

[GET] /artikel : menampilkan artikel dengan id tertentu

[POST] /artikel/insert : menambah artikel, contoh format:

```
{
    "params": [
        {
            "values": {
                "judul" : "TEs",
                "penulis" : "Ryan",
                "artikel" : "Ryannnn",
                "kategori": "Lifestyle",
                "email" : "ryanabdurohmanz@gmail.com"
            }
        }
    ]
}
```

[POST] /artikel/update : update artikel memakai postid

[POST] /artikel/delete : hapus artikel memakai postid
