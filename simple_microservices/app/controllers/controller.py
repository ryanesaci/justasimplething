from app.models.model import database
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqldb = database()

def showAll():
    dbres = mysqldb.showAll()
    res = []
    try:
        for item in dbres:
            user = {
                "postid" : item[0],
                "judul" : item[1],
                "penulis" : item[2],
                "artikel" : item[3],
                "kategori": item[4],
                "email" : item[5]
            }
            res.append(user)
    except TypeError:
        res.append({})
    
    return jsonify(res)

def showById(**params):
    dbres = mysqldb.showArtikelById(**params)
    user = {
        "postid" : item[0],
        "judul" : item[1],
        "penulis" : item[2],
        "artikel" : item[3],
        "kategori": item[4],
        "email": item[5]
    }
    return jsonify(user)

def insert(**params):
    for param in params["params"]:
        mysqldb.insertArtikel(**param["values"])
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def update(**params):
    for param in params["params"]:
        mysqldb.updateArtikelById(**param["values"])
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def delete(**params):
    mysqldb.deleteArtikelById(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def token(**params):
    dbres = mysqldb.showArtikelByEmail(**params)
    if dbres is not None:
        user = {
            "penulis" : dbres[2],
            "email" : dbres[5]
        }

        expires = datetime.timedelta(days=1)
        access_token = create_access_token(user, fresh=True, expire_delta=expires)
        data = {
                "data" : user,
                "token_access": access_token
                }
    else:
        data = {
                "message":"Email tidak terdaftar"
                }
    return jsonify(data)
