from mysql.connector import connect
import os

class database:
    def __init__(self):
        try:
            self.db = connect(host="localhost",#str(os.environ.get("HOST")),
                              user="root",#str(os.environ.get("USER")),
                              password="ryanesaci811")#str(os.environ.get("PASSWORD")))
            database=str(os.environ.get("DATABASE")),
            cursor = self.db.cursor()
            _SQL = """SHOW DATABASES"""
            cursor.execute(_SQL)
            results = cursor.fetchall()
            results_list = [item[0] for item in results]
            if "sanber" in results_list:
                cursor.execute("USE sanber;")
            else:
                cursor.execute("CREATE DATABASE sanber;")
                cursor.execute("USE sanber;")
            _SQL = """SHOW TABLES"""
            cursor.execute(_SQL)
            results = cursor.fetchall()
            results_list = [item[0] for item in results]
            if "simpleblog" not in results_list:
                q = """ 
                    CREATE TABLE simpleblog (
                        postid INT PRIMARY KEY AUTO_INCREMENT,
                        judul VARCHAR(512) NOT NULL,
                        penulis VARCHAR(512) NOT NULL,
                        artikel VARCHAR(5000) NOT NULL,
                        kategori VARCHAR(512) NOT NULL,
                        email VARCHAR(255) NOT NULL
                    );
                """
                cursor.execute(q)
            self.db.commit()
        except Exception as e:
            print(e)

    def showAll(self):
        try:
            cursor = self.db.cursor()
            q = '''select * from simpleblog;'''
            cursor.execute(q)
            res = cursor.fetchall()
            return res
        except Exception as e:
            print(e)

    def showArtikelById(self, **params):
        try:
            cursor = self.db.cursor()
            q = '''select * from simpleblog where userid={0};'''.format(params["postid"])
            cursor.execute(q)
            res = cursor.fetchone()
            return res
        except Exception as e:
            print(e)

    def showArtikelEmail(self, **params):
        try:
            cursor = self.db.cursor()
            q = '''select * from simpleblog where email={0};'''.format(params["email"])
            cursor.execute(q)
            res = cursor.fetchone()
            return res
        except Exception as e:
            print(e)

    def insertArtikel(self, **params):
        try:
            column = ', '.join(list(params.keys()))
            values = tuple(list(params.values()))
            q = ''' insert into simpleblog ({0}) values {1}; '''.format(column, values)
            cursor = self.db.cursor()
            cursor.execute(q)
        except Exception as e:
            print(e)
        
    def updateArtikelById(self, **params):
        try:
            postid = params['postid']
            values = self.restructureParams(**params['values'])
            crud_query = '''update simpleblog set {0} where postid = {1};'''.format(values, postid)
            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)
    
    def deleteUserById(self, **params):
        try:
            postid = params['postid']
            crud_query = '''delete from customers where userid = {0};'''.format(postid)
            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)
    
    def dataCommit(self):
        self.db.commit()
        
    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result



