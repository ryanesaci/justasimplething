from app import app
from app.controllers import controller
from flask import Blueprint, request

blog_blueprint = Blueprint("router", __name__)

@app.route("/artikels", methods=["GET"])
def showArticles():
    return controller.showAll()

@app.route("/artikel", methods=["GET"])
def showArticle():
    params = request.json
    return controller.showById(**params)

@app.route("/artikel/insert", methods=["POST"])
def insertArticle():
    params = request.json
    return controller.insert(**params)

@app.route("/artikel/update", methods=["POST"])
def updateArticle():
    params = request.json
    return controller.update(**params)

@app.route("/artikel/delete", methods=["POST"])
def deleteArticle():
    params = request.json
    return controller.delete(**params)

@app.route("/artikel/requesttoken", methods=["GET"])
def requesttoken():
    params = request.json
    return controller.token(**params)